<?php

defined('BASEPATH') or exit('No direct script access allowed');

class halaman extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model("M_toko", "Toko");
		$this->load->library('upload');
		$this->load->library('pagination');
		$this->load->helper('test');
	}

	public function index()
	{
		$product = file_get_contents('assets/datasource/products.json');
		$data['product'] = json_decode($product);
		$this->load->view('halaman/index', $data);


		// echo '<pre>';
		// print_r($data['product']);
		// exit();
	}



}
