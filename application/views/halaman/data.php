<table class="table">
	<thead>
		<tr>
			<th>NO.</th>
			<th>Nama Barang</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>
			<th>Stok</th>
			<th>Foto</th>
			<th class="text-center" style="width: 70px; vertical-align: middle"><i class="fas fa-cogs"></i></th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($toko) > 0) {
			$no = 1; ?>
			<?php foreach ($toko as $num => $row) { ?>
				<tr>
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo ucwords($row['nama_barang']); ?></td>
					<td>Rp.<?php echo numberToCurrency($row['harga_beli']); ?></td>
					<td>Rp.<?php echo numberToCurrency($row['harga_jual']); ?></td>
					<td><?php echo numberToCurrency($row['stok']); ?></td>
					<td><img src="<?php echo base_url($row['foto_barang']) ?>" style="height: 70px;" class="img-fluid" alt="Dokumen tidak ditemukan !"></td>
					<td class="text-center" style="width: 80px; vertical-align: middle">
						<a href="#!" class="btn-sm btn-icon btn-outline-warning" data-toggle="modal" data-target="#editData<?php echo $row['id_barang'] ?>"><i class="feather icon-edit"></i></a>
						<a href="#!" style="cursor: pointer" onclick="actControl('delete', '<?php echo $row['id_barang'] ?>')" class="btn-sm btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
					</td>

				</tr>


		<?php  }
		} else {
			echo '<tr><td colspan="9" class="text-center"><i class="fas fa-info-circle m-r-10"></i>Belum ada data </td></tr>';
		};
		?>
	</tbody>
</table>
<!-- pagination -->

<hr style="margin: 3px 0px 3px 0px; padding: 0px; margin-bottom: 10px" />
<div class="row">
	<div class="col-md-6">
		<ul class="pagination">
			<span class="paging-label" style="color:  #9f9f9f;">Menampilkan <?php echo  $pagination['start']; ?> - <?php echo  $pagination['end']; ?> dari total <?php echo  $pagination['total']; ?> data</span>
		</ul>
	</div>
	<div class="col-md-6">
		<nav class="float-right">
			<?php echo   $pagination['data']; ?>
		</nav>
	</div>
</div>
<!-- datatable start -->

<!-- datatable ends -->
<?php foreach ($toko as $num => $row) { ?>
	<div class="modal fade" id="editData<?php echo $row['id_barang'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="feather icon-map"></i> Edit Data Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="saving-data"></div>
				<?php echo validation_errors(); ?>
				<?php echo form_open('halaman/edit_proses', array('id' => 'edit', 'enctype' => 'multipart/form-data')); ?>
				<input type="hidden" name="id" value="<?php echo $row['id_barang']; ?>">
				<div class="modal-body text-left">
					<div class="form-group fill">
						<label>Nama Barang</label>
						<input type="text" class="form-control" name="nama_barang" value="<?php echo ucwords($row['nama_barang']); ?>" placeholder="Masukkan nama barang">
						<span><small id="nama_barang" class="text-danger text-left validasi" style="font-size:10px; width: 10rem;"></small></span>
					</div>
					<div class="form-group fill">
						<label>Harga beli
						</label>
						<input type="text" class="form-control" onkeyup="numberToCurrency(this);" value="<?php echo numberToCurrency($row['harga_beli']); ?>" name="harga_beli" placeholder="Masukkan harga beli">
						<span><small id="harga_beli" class="text-danger text-left" style="font-size:10px; width: 10rem;"></small></span>
					</div>
					<div class="form-group fill">
						<label>Harga Jual</label>
						<input type="text" class="form-control" onkeyup="numberToCurrency(this);" value="<?php echo numberToCurrency($row['harga_jual']); ?>" name="harga_jual" placeholder="Masukkan harga jual">
						<span><small id="harga_jual" class="text-danger text-left" style="font-size:10px; width: 10rem;"></small></span>
					</div>
					<div class="form-group fill">
						<label>Stok</label>
						<input type="text" class="form-control" name="stok" onkeyup="numberToCurrency(this);" value="<?php echo numberToCurrency($row['stok']); ?>" placeholder="Masukkan jumlah stok">
						<span><small id="stok" class="text-danger text-left" style="font-size:10px; width: 10rem;"></small></span>
					</div>
					<div class="form-group fill">
						<label>Foto Barang</label>
						<input type="file" class="form-control-file" name="foto" accept="image/png, image/gif, image/jpeg">
						<span><small id="stok" class="text-danger text-left" style="font-size:10px; width: 10rem;">upload, jika ingin mengubah file lama !</small></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-sm waves-effect waves-light btn-secondary" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn-sm waves-effect waves-light btn-primary">Simpan</button>
				</div>
				</form>
			</div>
		</div>
	</div>

<?php  } ?>
<script>
	//save id validasi dengan nama input harus sama
	$("#edit").submit(function(e) {
		e.preventDefault();
		let that = $(e.target)
		var ajaxData = new FormData(that.get(0));
		$.ajax({
			url: $(this).attr('action'),
			data: ajaxData,
			cache: false,
			contentType: false,
			processData: false,
			type: "POST",
			dataType: "JSON",
			success: function(json) {
				if (json.status == 0) {
					if (!json.msg) {
						$("#sav-form").html('');
						for (var x = 0; x < json.err.length; x++) {
							var sSts = $('.isi').val();
							if (sSts == "") {
								$('#' + json.err[x]['id']).html('');
							} else {
								$('#' + json.err[x]['id']).html(json.err[x]['err']);
							}
						}
					} else {
						$(".saving-data").html(json.msg);
						
					}
				} else {
					$(".saving-data").html(json.msg);
					setTimeout(function() {
						location.reload();
					}, 1500);
				}
			}
		});
		return false;
	});
</script>
