<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("wrapper/_partials/head.php") ?>
</head>

<body>
	<!-- [ Header ] start -->
	<?php $this->load->view("wrapper/_partials/header.php") ?>
	<!-- [ Header ] end -->
	<?php
	function get_reviews_summary($url_products, $url_reviews)
	{
		$products = json_decode(file_get_contents($url_products), true);
		$reviews = json_decode(file_get_contents($url_reviews), true);

		foreach ($products as $val) {
			$product_id = $val['id'];
			$total_reviews[$product_id] = array_count_values(array_column($reviews, 'product_id'))[$product_id];
			$five_star = array_count_values(array_column($reviews, 'rating'))[5];
			$four_star = array_count_values(array_column($reviews, 'rating'))[4];
			$three_star = array_count_values(array_column($reviews, 'rating'))[3];
			$two_star = array_count_values(array_column($reviews, 'rating'))[2];
			$one_star = array_count_values(array_column($reviews, 'rating'))[1];

			$ratings = array(
				1 => $one_star,
				2 => $two_star,
				3 => $three_star,
				4 => $four_star,
				5 => $five_star
			);
		}

		// cara itung rata2 all average bisa liat di sini rumusnya
		// https://www.kaskus.co.id/thread/000000000000000016658498/help-cara-menghitung-rata-rata-rating-website/
		$totalStars = 0;
		$voters = array_sum($ratings);
		foreach ($ratings as $stars => $votes) {
			$totalStars += $stars * $votes;
		}
		$average_ratings = number_format((float)$totalStars / $voters, 1, '.', '');

		$hasil_json = [
			"total_reviews" => $voters,
			"average_ratings" => $average_ratings,
			"5_star" => $five_star,
			"4_star" => $four_star,
			"3_star" => $three_star,
			"2_star" => $two_star,
			"1_star" => $one_star
		];

		return $hasil_json;
	}

	function get_reviews_product($url_products, $url_reviews, $product_id)
	{
		$data_products = json_decode(file_get_contents($url_products), true);
		$reviews = json_decode(file_get_contents($url_reviews), true);

		$reviews = array_filter($reviews, function ($data) use ($product_id) {
			return ($data['product_id'] === $product_id ? true : false);
		});

		$data_products = array_filter($data_products, function ($data) use ($product_id) {
			return ($data['id'] === $product_id ? true : false);
		});

		$total_reviews[$product_id] = array_count_values(array_column($reviews, 'product_id'))[$product_id];
		$five_star = array_count_values(array_column($reviews, 'rating'))[5];
		$four_star = array_count_values(array_column($reviews, 'rating'))[4];
		$three_star = array_count_values(array_column($reviews, 'rating'))[3];
		$two_star = array_count_values(array_column($reviews, 'rating'))[2];
		$one_star = array_count_values(array_column($reviews, 'rating'))[1];

		$ratings = array(
			1 => $one_star,
			2 => $two_star,
			3 => $three_star,
			4 => $four_star,
			5 => $five_star
		);

		// cara itung rata2 all average bisa liat di sini rumusnya
		// https://www.kaskus.co.id/thread/000000000000000016658498/help-cara-menghitung-rata-rata-rating-website/
		$totalStars = 0;
		$voters = array_sum($ratings);
		foreach ($ratings as $stars => $votes) {
			$totalStars += $stars * $votes;
		}
		$average_ratings = number_format((float)$totalStars / $voters, 1, '.', '');

		$hasil_json = [
			"total_reviews" => $voters,
			"average_ratings" => $average_ratings,
			"5_star" => $five_star,
			"4_star" => $four_star,
			"3_star" => $three_star,
			"2_star" => $two_star,
			"1_star" => $one_star
		];

		return $hasil_json;
	}

	?>
	<!-- [ Main Content ] start -->
	<div class="pcoded-main-container" style="margin-left: 0px;">
		<div class="pcoded-content">
			<!-- [ breadcrumb ] start -->
			<?php $this->load->view("wrapper/_partials/breadcrumb.php") ?>
			<!-- [ breadcrumb ] end -->
			<!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-body">
							<div class="card">
								<div class="card-body">
									<?php $summary = get_reviews_summary("assets/datasource/products.json", "assets/datasource/reviews.json"); ?>
									<div class="row">
										<div class="col-md-12">
											<h4>Review Summary</h4>
										</div>
										<div class="col">
											<label>Total Review</label>
											<p><?php echo $summary['total_reviews'] ?></p>
										</div>
										<div class="col">
											<label>Rating rata - rata</label>
											<p><?php echo $summary['average_ratings'] ?></p>
										</div>
										<div class="col">
											<label>Bintang 5</label>
											<p><?php echo $summary['5_star'] ?></p>
										</div>
										<div class="col">
											<label>Bintang 4</label>
											<p><?php echo $summary['4_star'] ?></p>
										</div>
										<div class="col">
											<label>Bintang 3</label>
											<p><?php echo $summary['3_star'] ?></p>
										</div>
										<div class="col">
											<label>Bintang 2</label>
											<p><?php echo $summary['2_star'] ?></p>
										</div>
										<div class="col">
											<label>Bintang 1</label>
											<p><?php echo $summary['1_star'] ?></p>
										</div>

									</div>
								</div>
							</div>
							<div class="col-md-12">
								<h4>Review Products</h4>
							</div>
							<?php if (count($product) > 0) {
								$no = 1; ?>
								<?php foreach ($product as $num => $row) { ?>
									<div class="col-md-12">
										<h5><?php echo $row->name ?></h5>
									</div>
									<div class="col">
										<label>Harga</label>
										<p><?php echo numberToCurrency($row->price) ?></p>
									</div>
									<div class="card">
										<div class="card-body">
											<?php $product = get_reviews_product("assets/datasource/products.json", "assets/datasource/reviews.json", $num + 1); ?> <div class="row">
												<div class="col">
													<label>Total Review</label>
													<p><?php echo $product['total_reviews'] ?></p>
												</div>
												<div class="col">
													<label>Rating rata - rata</label>
													<p><?php echo $product['average_ratings'] ?></p>
												</div>
												<div class="col">
													<label>Bintang 5</label>
													<p><?php echo $product['5_star'] ?></p>
												</div>
												<div class="col">
													<label>Bintang 4</label>
													<p><?php echo $product['4_star'] ?></p>
												</div>
												<div class="col">
													<label>Bintang 3</label>
													<p><?php echo $product['3_star'] ?></p>
												</div>
												<div class="col">
													<label>Bintang 2</label>
													<p><?php echo $product['2_star'] ?></p>
												</div>
												<div class="col">
													<label>Bintang 1</label>
													<p><?php echo $product['1_star'] ?></p>
												</div>

											</div>
										</div>
									</div>

							<?php  }
							} ?>
						</div>
					</div>
				</div>

			</div>
			<!-- [ Main Content ] end -->

		</div>



	</div>
	<!-- [ Main Content ] end -->
	<!-- Sticky Footer -->
	<?php $this->load->view("wrapper/_partials/footer.php") ?>
	<!-- Sticky Footer End -->


	<!-- Includes -->
	<?php $this->load->view("wrapper/_partials/scrolltop.php") ?>
	<?php $this->load->view("wrapper/_partials/modal.php") ?>
	<?php $this->load->view("wrapper/_partials/js.php") ?>


	<script>
		// select 2
		$('.select2').select2();

		// Fungsi currency
		function numberToCurrency(x) {
			var a = x.value;
			if ("" != a && null != a) {
				a = a.toString();

				a = a.replace(/[^\d\,]/g, "").split(",");
				for (var b = "", c = 0, d = a[0].length; 0 < d; d--)
					c += 1, b = 1 == c % 3 && 1 != c ? a[0].substr(d - 1, 1) + "." + b : a[0].substr(d - 1, 1) + b;
				1 < a.length && (b = 0 < a[1].length ? b + ("," + a[1]) : b + ",");
				$(x).val(b);
			}
			return "";
		}
	</script>

</body>

</html>
