<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_toko extends CI_Model {

    // public function all_data() {
    //     $sql = "SELECT a.*, b.prov_name, c.city_name, d.dis_name FROM toko a
    //                 LEFT JOIN provinces b ON b.prov_id = a.prov_id
    //                 LEFT JOIN cities c ON c.city_id = b.prov_id
	// 				LEFT JOIN districts d ON d.dis_id = c.city_id
    //                 ";
    //     $query = $this->db->query($sql);
    //     if ($query->num_rows() > 0) {
    //         $result = $query->result_array();
    //         $query->free_result();
    //         return $result;
    //     } else {
    //         return array();
    //     }
    // }
	// public function prov_data() {
    //     $sql = "SELECT * FROM provinces ";
    //     $query = $this->db->query($sql);
    //     if ($query->num_rows() > 0) {
    //         $result = $query->result_array();
    //         $query->free_result();
    //         return $result;
    //     } else {
    //         return array();
    //     }
    // }
	public function jumlah_data($nama_barang) {
	
        $sql = "SELECT * FROM barang WHERE nama_barang LIKE ? ";
        $query = $this->db->query($sql,$nama_barang);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	public function all_data($params) {
	
        $sql = "SELECT * FROM barang WHERE nama_barang LIKE ? 
		ORDER BY id_barang DESC
		LIMIT ?,?";
        $query = $this->db->query($sql,$params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	public function get_data_exist($barang) {
        $sql = "SELECT * FROM barang WHERE nama_barang LIKE ? ";
        $query = $this->db->query($sql,$barang);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

	public function dt($idx) {
        $sql = "SELECT * FROM barang WHERE id_barang LIKE ? ";
        $query = $this->db->query($sql,$idx);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
	


}

?>
